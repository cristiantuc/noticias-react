import React from 'react';
import { Button, Card } from 'react-bootstrap';

function Noticia({noticia}) {
  return(
    <Card border="info" className="w-25 mr-1 my-4">
      <Card.Img 
        variant="top" 
        src={(noticia["urlToImage"])} 
        className="border-bottom border-info"
      />
      <Card.Body>
        <Card.Title>{noticia["title"]}</Card.Title>
        <Card.Text>
          {(
            noticia["content"]===null ? 
            "No se encuentra el contenido" :
            noticia["content"].slice(0, noticia["content"].indexOf("["))
          )}
        </Card.Text>
        <Button 
          variant="info" 
          href={noticia["url"]} 
          target="_blank"
        >
          Leer nota
        </Button>
      </Card.Body>
    </Card>   
  );
}

export default Noticia;