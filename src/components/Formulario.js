import React from 'react';
import { Form } from 'react-bootstrap';

function Formulario({etiqueta, opciones, onChange}) {
  return(
    <Form>
      <Form.Group className="d-flex flex-row justify-content-start pb-2 pt-3 pl-4">
        <Form.Label className="my-0 mx-3">
          <h5>{etiqueta}</h5>
        </Form.Label>
        <Form.Control 
          size="sm" 
          as="select" 
          className="border-info my-0 mr-3 col-5"
          onChange={onChange}
          style={{fontWeight: 'bold'}}
        >
          {
            opciones.map((opcion, index) => {
              return (<option key={index} value={opcion}>{opcion}</option>);
            })
          }
        </Form.Control>
      </Form.Group>
    </Form>
  );
}

export default Formulario;