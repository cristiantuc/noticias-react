import React, { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import Formulario from './Formulario';
import Noticia from './Noticia';

function ListaNoticias() {
  const API_KEY = 'a5947954765547ac9328b7be62745a56';
  const paises = ['ar', 'au', 'at', 'be', 'br', 'bg', 'ca', 'cn', 'co', 'cu', 'cz', 'eg', 'fr', 'de', 'gr', 'hk', 'hu', 'in', 'id', 'ie', 'il', 'i', 'jp', 'lv', 'lt', 'my', 'mx', 'ma', 'nl', 'nz', 'ng', 'no', 'ph', 'pl', 'pt', 'ro', 'ru', 'sa', 'rs', 'sg', 'sk', 'si', 'za', 'kr', 'se', 'ch', 'tw', 'th', 'tr', 'ae', 'ua', 'gb', 'us', 've'];
  const paisesEsp =['Argentina', 'Australia', 'Austria', 'Bélgica', 'Brasil', 'Bulgaria', 'Canadá', 'China', 'Colombia', 'Cuba', 'República Checa', 'Egipto', 'Francia', 'Alemania', 'Grecia', 'Hong Kong', 'Hungría', 'India', 'Indonesia', 'Irlanda', 'Israel', 'Italia ', 'Japón', 'Letonia', 'Lituania', 'Malasia', 'México', 'Marruecos', 'Países Bajos', 'Nueva Zelanda', 'Nigeria', 'Noruega', 'Filipinas', 'Polonia', 'Portugal', 'Rumanía', 'Rusia', 'Arabia Saudita', 'Serbia', 'Singapur', 'Eslovaquia', 'Eslovenia' , 'Sudáfrica', 'Corea del Sur', 'Suecia', 'Suiza', 'Taiwán', 'Tailandia', 'Turquía', 'Emiratos Árabes Unidos', 'Ucrania', 'Reino Unido', 'Estados Unidos', 'Venuzuela'];
  const categorias = ['general', 'business', 'entertainment', 'health', 'science', 'sports', 'technology'];
  const categoriasEsp = ['General', 'Negocios', 'Entretenimiento', 'Salud', 'Ciencia', 'Deportes', 'Tecnología'];
  const [paisSelec, setPaisSelec] = useState([paises[0]]);
  const [cateSelec, setCateSelec] = useState(categorias[0]);
  const [noticias, setNoticias] = useState([]);
 
  const guardarPaisSelec = (e) => {
    setPaisSelec(
      paises[
        paisesEsp.indexOf(e.target.value)
      ]
    );
  }

  const guardarCateSelec = (e) => {
    setCateSelec(
      categorias[
        categoriasEsp.indexOf(e.target.value)
      ]
    );
  }

  useEffect(() => {
    async function fetchData() {
      const solicitud = await fetch(
        `http://newsapi.org/v2/top-headlines?country=${paisSelec}&category=${cateSelec}&pageSize=20&apiKey=${API_KEY}`);
      const respuesta = await solicitud.json();
      setNoticias(respuesta["articles"]);
    }
    fetchData();
  }, [paisSelec, cateSelec])

  return(
    <Container className="border border-info col-8 pt-4 px-0 mb-4">
      <Formulario
        etiqueta="Elija un país:"
        opciones={paisesEsp} 
        onChange={guardarPaisSelec}
      />
      <Formulario
        etiqueta="Elija una categoría:"
        opciones={categoriasEsp} 
        onChange={guardarCateSelec}
      />
      <Container className="d-flex flex-row flex-wrap justify-content-around border-top border-info mx-0">
        {
          noticias.map((noticia, index) => {
            return(
              <Noticia key={index} noticia={noticia}/>
            );
          })
        }
      </Container>
    </Container>
  );
}

export default ListaNoticias;