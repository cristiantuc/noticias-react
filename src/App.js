import React from 'react';
import Header from './components/Header';
import ListaNoticias from './components/ListaNoticias';
import 'bootstrap/dist/css/bootstrap.min.css';  

function App() {
  return (
    <div>
     <Header/>
     <ListaNoticias/>
    </div>
  );
}

export default App;
