import React from 'react';
import { Alert, Container } from 'react-bootstrap';

function Header() {
  return(
    <Container fluid>
      <h1>
        <Alert 
          className="border border-info rounded-0 border-top-0 border-left-0 border-right-0 pb-4 d-flex justify-content-center" 
          variant="info"
        >
          Noticias
        </Alert>
      </h1>
    </Container>
  )
}

export default Header;